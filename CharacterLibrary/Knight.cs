﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
    public class Knight : Fighter
    {
        public Knight(string name)
        {
            Name = name;
            HP = 120;
            MaxHP = 120;
            Stamina = 100;
            ArmorRating = 12;
            Strength = 14;
        }
        public override void Attack()
        {
            Console.WriteLine("...");
        }
    }
}
