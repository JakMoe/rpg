﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
    public class Warlock : Wizard
    {
        public Warlock(string name)
        {
            Name = name;
            HP = 80;
            MaxHP = 80;
            Stamina = 100;
            ArmorRating = 7;
            Intelligence = 11;
        }

        public override void Attack()
        {
            Console.WriteLine("...");
        }
    }
}
