﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
    public class Archer : Fighter
    {
        public Archer(string name)
        {
            Name = name;
            HP = 90;
            MaxHP = 90;
            Stamina = 100;
            ArmorRating = 6;
            Strength = 10;
        }
        public override void Attack()
        {
            Console.WriteLine("...");
        }
    }
}
