﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
     public class Assassin : Rogue
    {
        public Assassin(string name)
        {
            Name = name;
            HP = 70;
            MaxHP = 70;
            Stamina = 100;
            ArmorRating = 5;
            Agility = 14;
        }
        public override void Attack()
        {
            Console.WriteLine("...");
        }
    }
}
