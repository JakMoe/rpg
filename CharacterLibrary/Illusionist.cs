﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
    public class Illusionist : Wizard
    {
        public Illusionist(string name)
        {
            Name = name;
            HP = 60;
            MaxHP = 60;
            Stamina = 70;
            ArmorRating = 2;
            Intelligence = 14;
        }
        public override void Attack()
        {
            Console.WriteLine("...");
        }
    }
}
