﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
   public class Elementalist : Wizard
    {
        public Elementalist(string name)
        {
            Name = name;
            HP = 80;
            MaxHP = 80;
            Stamina = 50;
            ArmorRating = 6;
            Intelligence = 12;
        }
        public override void Attack()
        {
            Console.WriteLine("...");
        }
    }
}
