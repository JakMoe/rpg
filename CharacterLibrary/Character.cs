﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int HP { get; set; }
        public int MaxHP { get; set; }

        public int Stamina { get; set; }
        public int ArmorRating { get; set; }

        public int Intelligence { get; set; } = 10;

        public int Strength { get; set; } = 10; 
        public int Agility { get; set; } = 10;

        public int THAR0 { get => 20 - ((Strength - 10) / 2); }

        public abstract void Attack();
        public void Move(int distance)
        {
            Console.WriteLine("Move");
        }
        public int TakeDamage(int amount)
        {
            HP = HP - amount;
            return HP;
        }
        public bool Dead()
        {
            if(HP == 0)
            {
                return true;
            }else
            {
                return false;
            }
        }

        
    }
}
