﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary
{
    public class Thief : Rogue
    {
        public Thief(string name)
        {
            Name = name;
            HP = 70;
            MaxHP = 70;
            Stamina = 140;
            ArmorRating = 6;
            Agility = 13;
        }

        public override void Attack()
        {
            Console.WriteLine("...");
        }
    }
}
