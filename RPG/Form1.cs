﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CharacterLibrary;
using Newtonsoft.Json;
using System.Data.SqlClient;


namespace RPG
{
    public partial class Form1 : Form
    {

        public string json;
        public string charName;

        private static void AddCharacter(Character c)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"PC7377\SQLEXPRESS";
            builder.InitialCatalog = "RPG_Character";
            builder.IntegratedSecurity = true;
            Console.WriteLine(builder.ConnectionString);

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    Console.WriteLine("Done. \n");
                    string sql = "INSERT INTO Character (Full_Name, HP,Stamina,ArmorRating,Intelligence,Strength,Agility) " +
                        $"Values('{c.Name}',{c.MaxHP}, {c.Stamina},{c.ArmorRating},{c.Intelligence},{c.Strength},{c.Agility})";
                    Console.WriteLine(sql);
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }

                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
    
         public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Console.WriteLine(json);
            string name = charName;
            Console.WriteLine(JsonConvert.DeserializeObject(json));
            System.IO.File.WriteAllText(@"C:\Users\jahmoe\source\repos\RPG\RPG\" + charName + ".txt", json);


        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string characterName = textBox1.Text;
            Console.WriteLine(characterName);

        }
        public void RemoveText(object sender, EventArgs e)
        {
            if (textBox1.Text == "Enter name")
            {
                textBox1.Text = "";
            }
        }
        public void AddText(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                textBox1.Text = "Enter name";
            }
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButton4.Text = "Illusionist";
            radioButton4.Visible = true;
            radioButton5.Text = "Elementalist";
            radioButton5.Visible = true;
            radioButton6.Text = "Warlock";
            radioButton6.Visible = true;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButton4.Text = "Thief";
            radioButton4.Visible = true;
            radioButton5.Text = "Assassin";
            radioButton5.Visible = true;
            radioButton6.Visible = false;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            radioButton4.Text = "Archer";
            radioButton4.Visible = true;
            radioButton5.Text = "Knight";
            radioButton5.Visible = true;
            radioButton6.Visible = false;
        }
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            
            if (radioButton4.Checked) { 
            string name = textBox1.Text;
            switch (radioButton4.Text.ToLower())
            {
                case "illusionist":
                    var illisionist1 = new Illusionist(name);
                    label5.Text = illisionist1.Name;
                    label6.Text = (illisionist1.MaxHP*10).ToString() + "/" + (illisionist1.HP*10).ToString();
                    label8.Text = illisionist1.Strength.ToString();
                    label9.Text = "Mana";
                    label10.Text = illisionist1.Stamina.ToString();
                    label12.Text = illisionist1.ArmorRating.ToString();
                    label20.Text = illisionist1.Intelligence.ToString();
                    label16.Text = illisionist1.GetType().ToString().Split('.').Last();
                    label19.Text = illisionist1.Agility.ToString();
                    json = JsonConvert.SerializeObject(illisionist1);
                    charName = illisionist1.Name; ;
                    AddCharacter(illisionist1);
                        break;
                case "thief":
                    var thief1 = new Thief(name);
                    label5.Text = thief1.Name;
                    label6.Text = (thief1.MaxHP * 10).ToString() + "/" + (thief1.HP * 10).ToString();
                    label8.Text = thief1.Strength.ToString();
                    label9.Text = "Energy";
                    label10.Text = thief1.Stamina.ToString();
                    label12.Text = thief1.ArmorRating.ToString();
                    label20.Text = thief1.Intelligence.ToString();
                    label16.Text = thief1.GetType().ToString().Split('.').Last();
                    label19.Text = thief1.Agility.ToString();
                    json = JsonConvert.SerializeObject(thief1);
                    charName = thief1.Name;
                    AddCharacter(thief1);
                        break;
                case "archer":
                    var archer1 = new Archer(name);
                    label5.Text = archer1.Name;
                    label6.Text = (archer1.MaxHP * 10).ToString() + "/" + (archer1.HP * 10).ToString();
                    label9.Text = "Stamina";
                    label8.Text = archer1.Strength.ToString();
                    label10.Text = archer1.Stamina.ToString();
                    label12.Text = archer1.ArmorRating.ToString();
                    label20.Text = archer1.Intelligence.ToString();
                    label16.Text = archer1.GetType().ToString().Split('.').Last();
                    label19.Text = archer1.Agility.ToString();
                    json = JsonConvert.SerializeObject(archer1);
                    charName = archer1.Name;
                    AddCharacter(archer1);
                        break;
                default:
                    break;
            }
            tableLayoutPanel1.Visible = true;
            }

        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton6.Checked)
            {
                string name = textBox1.Text;
                var warlock = new Warlock(name);
                label5.Text = warlock.Name;
                label6.Text = (warlock.MaxHP * 10).ToString() + "/" + (warlock.HP * 10).ToString();
                label8.Text = warlock.Strength.ToString();
                label9.Text = "Mana";
                label10.Text = warlock.Stamina.ToString();
                label12.Text = warlock.ArmorRating.ToString();
                label20.Text = warlock.Intelligence.ToString();
                label16.Text = warlock.GetType().ToString().Split('.').Last();
                label19.Text = warlock.Agility.ToString();
                json = JsonConvert.SerializeObject(warlock);
                tableLayoutPanel1.Visible = true;
                charName = warlock.Name;
                AddCharacter(warlock);
            }
        }
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                string name = textBox1.Text;
                switch (radioButton5.Text.ToLower())
                {
                    case "elementalist":
                        var elementist1 = new Elementalist(name);
                        label5.Text = elementist1.Name;
                        label6.Text = (elementist1.MaxHP * 10).ToString() + "/" + (elementist1.HP * 10).ToString();
                        label9.Text = "Mana";
                        label10.Text = elementist1.Stamina.ToString();
                        label12.Text = elementist1.ArmorRating.ToString();
                        label20.Text = elementist1.Intelligence.ToString();
                        label16.Text = elementist1.GetType().ToString().Split('.').Last();
                        label19.Text = elementist1.Agility.ToString();
                        json = JsonConvert.SerializeObject(elementist1);
                        charName = elementist1.Name;
                        AddCharacter(elementist1);
                        break;
                    case "assassin":
                        var assassin1 = new Assassin(name);
                        label5.Text = assassin1.Name;
                        label6.Text = (assassin1.MaxHP * 10).ToString() + "/" + (assassin1.HP * 10).ToString();
                        label8.Text = assassin1.Strength.ToString();
                        label9.Text = "Energy";
                        label10.Text = assassin1.Stamina.ToString();
                        label12.Text = assassin1.ArmorRating.ToString();
                        label20.Text = assassin1.Intelligence.ToString();
                        label16.Text = assassin1.GetType().ToString().Split('.').Last();
                        label19.Text = assassin1.Agility.ToString();
                        json = JsonConvert.SerializeObject(assassin1);
                        charName = assassin1.Name;
                        AddCharacter(assassin1);
                        break;
                    case "knight":
                        var knight = new Knight(name);
                        label5.Text = knight.Name;
                        label6.Text = (knight.MaxHP * 10).ToString() + "/" + (knight.HP * 10).ToString();
                        label8.Text = knight.Strength.ToString();
                        label9.Text = "Stamina";
                        label10.Text = knight.Stamina.ToString();
                        label12.Text = knight.ArmorRating.ToString();
                        label20.Text = knight.Intelligence.ToString();
                        label16.Text = knight.GetType().ToString().Split('.').Last();
                        label19.Text = knight.Agility.ToString();
                        json = JsonConvert.SerializeObject(knight);
                        charName = knight.Name;
                        AddCharacter(knight);
                        break;
                    default:
                        break;
                }
                tableLayoutPanel1.Visible = true;
            }
        }


    }
}
