# RPG Character creator

## Description

Console app where the user can create his own Character or display characters 
that allready are made.
## Getting Started

Open it in Visual studio, hit run.
follow instructions in app.

## Authors

* **Jakob Hauge Moe** - *Backend work* - [JakMoe](https://gitlab.com/JakMoe)